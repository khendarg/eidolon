#!/usr/bin/env python

import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from matplotlib.patches import Ellipse
import matplotlib.patches as patches
from matplotlib.text import Annotation
from matplotlib.path import Path

import shlex
import argparse
DENSITY = 1.37 #g/cm^3
DENSITY /= 1e8**3 #->g/\AA^3
DENSITY *= 6.022e23 #->Da/\AA^3
RES_MASS = 110 #mean aa. mass (Da)
RES_VOL = RES_MASS / DENSITY #mean aa. volume (\AA^3)
loopsmoother = lambda x: x**1.0
domsmoother = lambda x: x**1.0
def aa2ellipsoid(length, aspect=1.):
    pass

class Domain(object):
    def __init__(self, pos=(0,0), length=100, aspect=None, ec='black', fc='gray', label='', anchor=None, face=-1):
        self.pos = pos
        self.length = length
        self.aspect = aspect
        self.ec = ec
        self.fc = fc
        self.label = label
        self.face = face
        self.anchor = anchor
    @staticmethod
    def from_text(args):
        obj = Domain()
        obj.anchor = args[0]
        obj.length = int(args[2]) - int(args[1])
        if len(args) > 3:
            obj.aspect = float(args[3])
        return obj
    def get_dims(self):
        vol = RES_VOL * self.length
        r = (vol*3/4)**(1./3)
        return (domsmoother(r*self.aspect**.5), domsmoother(r/self.aspect**.5))
        
    def render(self, ax):
        if self.aspect is None: aspect = 1.0
        else: aspect = self.aspect
        dims = self.get_dims()
        ellipse = Ellipse(self.pos, width=dims[0], height=dims[1], fc=self.fc, ec=self.ec)
        ax.add_patch(ellipse)
        if self.face < 0: va = 'top'
        elif self.face > 0: va = 'bottom'
        else: va = 'center'
        label = ax.text(self.pos[0], self.pos[1], self.label, va=va, ha='center')
        return ellipse
        
class Terminal(object):
    def __init__(self, start=(0,0), end=(0,5), ec='black', fc='gray', weight=4, face=-1, length=3, label='', lw=1):
        self.start = start
        self.end = end
        self.ec = ec
        self.fc = fc
        self.weight = weight
        self.face = face
        self.length = length
        self.label = label
        self.lw = lw
    @staticmethod
    def from_text(args):
        obj = Terminal()
        obj.length = int(args[1]) - int(args[0]) + 1
        #obj.length = smooth(obj.length)
        if args[2] == 'out': obj.face = 1
        else: obj.face = -1
        return obj

    def get_height(self):
        return loopsmoother(self.length)*3/2

    def render(self, ax): 
        verts = [
            self.start,
            (self.start[0], self.end[1]+self.get_height()*self.face),
            (self.start[0], self.end[1]+self.get_height()*self.face),
            (self.end[0], self.end[1]+self.get_height()*self.face)
        ]
        codes = [
            Path.MOVETO,
            Path.CURVE4,
            Path.CURVE4,
            Path.CURVE4,
        ]
        path = Path(verts, codes)
        outerpatch = patches.PathPatch(path, facecolor=(0,0,0,0), ec=self.ec, lw=self.weight+self.lw+1)
        innerpatch = patches.PathPatch(path, facecolor=(0,0,0,0), ec=self.fc, lw=self.weight)
        ax.add_patch(outerpatch)
        ax.add_patch(innerpatch)
 
class TMH(object):
    def __init__(self, pos=(0,0), width=2.3, height=30, ec='black', fc='orange', length=30, label=''):
        self.pos = pos
        self.width = width
        self.height = height
        self.ec = ec
        self.fc = fc
        self.label = label
    @staticmethod
    def from_text(args):
        obj = TMH()
        return obj
    def render(self, ax):
        rect = Rectangle((self.pos[0] - self.width/2, self.pos[1] - self.height/2),
            self.width, self.height,
            fc=self.fc,
            ec=self.ec,
        )
        ax.add_patch(rect)
        label = ax.text(self.pos[0], 0, self.label, va='center', ha='center', rotation='vertical')
        return rect, label

class Loop(object):
    def __init__(self, start=(0,0), end=(5,0), ec='black', fc='gray', weight=4, label='', face=-1, lw=1, length=5):
        self.start = start
        self.end = end
        self.ec = ec
        self.fc = fc
        self.weight = weight
        self.label = label
        self.face = face
        self.lw = lw
        self.length = length
    @staticmethod
    def from_text(args):
        obj = Loop()
        obj.length = int(args[1]) - int(args[0])
        #obj.length = smooth(obj.length)
        return obj
    def get_height(self):
        return loopsmoother(self.length)*3/2
    def render(self, ax): 
        verts = [
            self.start,
            (self.start[0], self.start[1]+self.get_height()*self.face),
            (self.end[0], self.end[1]+self.get_height()*self.face),
            self.end
        ]
        codes = [
            Path.MOVETO,
            Path.CURVE4,
            Path.CURVE4,
            Path.CURVE4,
        ]
        path = Path(verts, codes)
        outerpatch = patches.PathPatch(path, facecolor=(0,0,0,0), ec=self.ec, lw=self.weight+self.lw+1)
        innerpatch = patches.PathPatch(path, facecolor=(0,0,0,0), ec=self.fc, lw=self.weight)
        ax.add_patch(outerpatch)
        ax.add_patch(innerpatch)
        

def color(args, cfg):
    if not args: return
    if len(args) == 1:
        for e in cfg['entities']:
            cfg['entities'][e].fc = args[0]
    else:
        for e in args[1:]:
            cfg['entities'][e].fc = args[0]
def cfgset(args, cfg):
    if len(args) < 2: raise TypeError('set: Not enough arguments: {}'.format(args))
    elif args[0] == 'title': cfg['title'] = args[1]
    else: raise ValueError('set: Unknown setting: {}'.format(args))
        
def blank_cfg():
    cfg = {}
    cfg['tmspacing'] = 1.
    cfg['title'] = ''
    cfg['save'] = ''
    cfg['show'] = False
    cfg['entities'] = {}
    cfg['tms'] = []
    cfg['loop'] = []
    cfg['domain'] = []
    cfg['face'] = -1
    return cfg
def parse_cfg(f, cfg):
    for lineno, l in enumerate(f):
        if not l.strip(): continue
        elif l.startswith('#'): continue

        args = shlex.split(l.strip())

        if len(args) < 1: raise ValueError('Not enough arguments at line {}'.format(lineno+1))
        elif args[0] == 'show': cfg['show'] = True
        elif len(args) < 2: raise ValueError('Not enough arguments at line {}'.format(lineno+1))
        elif args[0] == 'color':
            color(args[1:], cfg)
        elif args[0] == 'save':
            cfg['save'] = args[1]
        elif args[0] == 'set':
            cfgset(args[1:], cfg)
        elif len(args) < 3: raise ValueError('Not enough arguments at line {}'.format(lineno+1))
        elif args[0] == 'tmh':
            entity = TMH.from_text(args[2:])
            entity.label = args[1]
            cfg['face'] *= -1
            cfg['entities'][args[1]] = entity
            cfg['tms'].append(entity)
        elif args[0] == 'loop':
            entity = Loop.from_text(args[2:])
            entity.face = cfg['face']
            cfg['entities'][args[1]] = entity
            cfg['loop'].append(entity)
        elif args[0] == 'domain':
            entity = Domain.from_text(args[2:])
            entity.label = args[1]
            cfg['entities'][args[1]] = entity
            cfg['domain'].append(entity)
        elif len(args) < 4: raise ValueError('Not enough arguments at line {}'.format(lineno+1))
        elif args[0] == 'terminal':
            entity = Terminal.from_text(args[2:])
            cfg['face'] = entity.face
            cfg['entities'][args[1]] = entity
            cfg['loop'].append(entity)
        elif len(args) < 5: raise ValueError('Not enough arguments at line {}'.format(lineno+1))
    return cfg

def build_figure(cfg):
    x = 0
    oldx = 0
    nextx = 0
    leftmost = 0
    rightmost = 0
    topmost = 0
    bottommost = 0
    tmx = []
    for i, tms in enumerate(cfg['tms']): 
        nextx = x + (cfg['tmspacing'] + tms.width)
        if not i: oldx = x - (cfg['tmspacing'] + tms.width)
        if nextx > rightmost: rightmost = nextx
        if oldx < leftmost: leftmost = oldx
        tmx.append(oldx)

        tms.pos = (x, 0)

        if (tms.pos[1] + tms.height/2) > topmost: 
            topmost = tms.pos[1] + tms.height/2
        if (tms.pos[1] - tms.height/2) < bottommost: 
            bottommost = tms.pos[1] - tms.height/2
        oldx = x
        x = nextx
    tmx.append(oldx)
    tmx.append(x)
    leftmost = min(leftmost, min(tmx))
    rightmost = max(rightmost, max(tmx))

    for i, loop in enumerate(cfg['loop']):
        if type(loop) is Loop:
            #TODO: change 15, make it dynamic
            loop.start = (tmx[i], loop.face*15)
            loop.end = (tmx[i+1], loop.face*15)
            if loop.face == 1: topmost = max(15 + loop.get_height(), topmost)
            elif loop.face == -1: bottommost = min(-15 - loop.get_height(), bottommost)
        if type(loop) is Terminal:
            loop.start = (tmx[i], loop.face*15)
            loop.end = (tmx[i+1], loop.face*15)
            loop.end = ((loop.start[0]+loop.end[0])/2, loop.end[1])
            if i <= len(cfg['tms'])/2:
                loop.start = (tmx[i+1], loop.face*15)
                loop.end = ((tmx[i]+tmx[i+1])/2, loop.face*15)
            else:
                loop.start = (tmx[i], loop.face*15)
                loop.end = ((tmx[i]+tmx[i+1])/2, loop.face*15)
            if loop.face == 1: topmost = max(15 + loop.get_height(), topmost)
            elif loop.face == -1: bottommost = min(-15 - loop.get_height(), bottommost)

    for i, domain in enumerate(cfg['domain']):
        anchor = cfg['entities'][domain.anchor]
        if type(anchor) is Loop:
            pos = [(anchor.start[0]+anchor.end[0])/2, (anchor.start[1]+anchor.end[1])/2]
            pos[1] += anchor.face * anchor.get_height()/2
            domain.pos = pos
            if domain.aspect is None: domain.aspect = len(cfg['tms'])**-.5
            domain.face = anchor.face
        elif type(anchor) is Terminal:
            pos = [(anchor.start[0]+anchor.end[0])/2, (anchor.start[1]+anchor.end[1])/2]
            pos[1] += anchor.face * anchor.get_height()/2
            domain.pos = pos
            if domain.aspect is None: domain.aspect = len(cfg['tms'])**-.5
            domain.face = anchor.face

    fig = plt.figure()
    ax = fig.add_subplot(111)
    for e in cfg['entities']:
        cfg['entities'][e].render(ax)
    ax.set_xlim(leftmost, rightmost)
    ax.set_ylim(bottommost, topmost)
    ax.set_axis_off()
    ax.set_title(cfg['title'])
    fig.set_tight_layout(True)
    if cfg['save']: fig.savefig(cfg['save'])
    if cfg['show']: plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', nargs='+')
    args = parser.parse_args()

    cfg = blank_cfg()
    for fn in args.infile:
        with open(fn) as f:
            parse_cfg(f, cfg)
    if not cfg['save']: cfg['show'] = True
    build_figure(cfg)

